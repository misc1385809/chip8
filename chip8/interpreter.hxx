#ifndef CHIP8_INTERPRETER
#define CHIP8_INTERPRETER

#include <array>
#include <cstdint>
#include <cstdlib>
#include <limits>
#include <random>
#include <span>

namespace chip8
{
    /// @brief The CHIP-8 interpreter.
    class interpreter
    {
    public:
        /// @brief Construct an interpreter object.
        /// @param rom The ROM memory.
        interpreter(const std::span<std::uint8_t>& rom)
        {
            std::copy(_font.begin(), _font.end(), _memory.begin());
            std::copy(rom.begin(), rom.begin() + 3584, _memory.begin() + 512);
        }

        /// @brief Emulate one CPU cycle.
        void cycle()
        {
            std::uint16_t opcode = (_memory[_counter] << 8) | _memory[_counter + 1];
            jmptbl_nnnn(opcode);
        }

    private:
        /// @brief The pre-defined font data.
        inline static constexpr std::array<uint8_t, 80> _font
        {
            0xF0, 0x90, 0x90, 0x90, 0xF0, // 0
            0x20, 0x60, 0x20, 0x20, 0x70, // 1
            0xF0, 0x10, 0xF0, 0x80, 0xF0, // 2
            0xF0, 0x10, 0xF0, 0x10, 0xF0, // 3
            0x90, 0x90, 0xF0, 0x10, 0x10, // 4
            0xF0, 0x80, 0xF0, 0x10, 0xF0, // 5
            0xF0, 0x80, 0xF0, 0x90, 0xF0, // 6
            0xF0, 0x10, 0x20, 0x40, 0x40, // 7
            0xF0, 0x90, 0xF0, 0x90, 0xF0, // 8
            0xF0, 0x90, 0xF0, 0x10, 0xF0, // 9
            0xF0, 0x90, 0xF0, 0x90, 0x90, // A
            0xE0, 0x90, 0xE0, 0x90, 0xE0, // B
            0xF0, 0x80, 0x80, 0x80, 0xF0, // C
            0xE0, 0x90, 0x90, 0x90, 0xE0, // D
            0xF0, 0x80, 0xF0, 0x80, 0xF0, // E
            0xF0, 0x80, 0xF0, 0x80, 0x80  // F
        };

        /// @brief The default random engine.
        inline static std::default_random_engine _rand{std::random_device{}()};

        /// @brief The uniform int distribution.
        inline static std::uniform_int_distribution<std::uint8_t> _dist
        {
            std::numeric_limits<std::uint8_t>::min(),
            std::numeric_limits<std::uint8_t>::max()
        };

        /// @brief The CHIP-8 memory.
        std::array<std::uint8_t, 4096> _memory{};

        /// @brief The pixel data.
        std::array<std::uint8_t, 2048> _gfx{};

        /// @brief The call stack.
        std::array<std::uint16_t, 16> _stack{};

        /// @brief The active inputs.
        std::array<std::uint8_t, 16> _input{};

        /// @brief The CHIP-8 registers.
        std::array<std::uint8_t, 16> _registers{};

        /// @brief An index to the current stack position.
        std::size_t _stack_ptr{};

        /// @brief The address register.
        std::uint16_t _addr_ptr{};

        /// @brief The program counter.
        std::uint16_t _counter{0x200};

        /// @brief The delay timer which counts down at 60Hz.
        std::uint8_t _delay_timer{};

        /// @brief The sound timer which emits a sound when it reaches zero.
        std::uint8_t _sound_timer{};

        /// @brief Starting jump table for opcode.
        /// @param opcode The opcode.
        void jmptbl_nnnn(std::uint16_t opcode)
        {
            switch (opcode & 0xF000)
            {
                case 0x0000:
                    jmptbl_00nn(opcode);
                    break;
                
                case 0x1000:
                    opcode_1nnn(opcode);
                    break;

                case 0x2000:
                    opcode_2nnn(opcode);
                    break;

                case 0x3000:
                    opcode_3xnn(opcode);
                    break;

                case 0x4000:
                    opcode_4xnn(opcode);
                    break;

                case 0x5000:
                    opcode_5xy0(opcode);
                    break;

                case 0x6000:
                    opcode_6xnn(opcode);
                    break;

                case 0x7000:
                    opcode_7xnn(opcode);
                    break;

                case 0x8000:
                    jmptbl_8xyn(opcode);
                    break;

                case 0x9000:
                    opcode_9xy0(opcode);
                    break;

                case 0xA000:
                    opcode_annn(opcode);
                    break;

                case 0xB000:
                    opcode_bnnn(opcode);
                    break;

                case 0xC000:
                    opcode_cxnn(opcode);
                    break;

                case 0xD000:
                    opcode_dxyn(opcode);
                    break;

                case 0xE000:
                    jmptbl_exnn(opcode);
                    break;

                case 0xF000:
                    jmptbl_fxnn(opcode);
                    break;
            }
        }

        /// @brief Jump table for 0x00NN opcodes.
        /// @param opcode The opcode.
        void jmptbl_00nn(std::uint16_t opcode)
        {
            switch (opcode)
            {
                case 0x00E0:
                    opcode_00e0(opcode);
                    break;

                case 0x00EE:
                    opcode_00ee(opcode);
                    break;
            }
        }

        /// @brief Jump table for 0x8XYN opcodes.
        /// @param opcode The opcode.
        void jmptbl_8xyn(std::uint16_t opcode)
        {
            switch (opcode & 0x000F)
            {
                case 0x0000:
                    opcode_8xy0(opcode);
                    break;

                case 0x0001:
                    opcode_8xy1(opcode);
                    break;

                case 0x0002:
                    opcode_8xy2(opcode);
                    break;

                case 0x0003:
                    opcode_8xy3(opcode);
                    break;

                case 0x0004:
                    opcode_8xy4(opcode);
                    break;

                case 0x0005:
                    opcode_8xy5(opcode);
                    break;

                case 0x0006:
                    opcode_8xy6(opcode);
                    break;

                case 0x0007:
                    opcode_8xy7(opcode);
                    break;

                case 0x000E:
                    opcode_8xye(opcode);
                    break;
            }
        }

        /// @brief Jump table for 0xEXNN.
        /// @param opcode The opcode.
        void jmptbl_exnn(std::uint16_t opcode)
        {
            switch (opcode & 0x00FF)
            {
                case 0x009E:
                    opcode_ex9e(opcode);
                    break;

                case 0x00A1:
                    opcode_exa1(opcode);
                    break;
            }
        }

        /// @brief Jump table for 0xFXNN.
        /// @param opcode The opcode.
        void jmptbl_fxnn(std::uint16_t opcode)
        {
            switch (opcode & 0x00FF)
            {
                case 0x0007:
                    opcode_fx07(opcode);
                    break;

                case 0x000A:
                    opcode_fx0a(opcode);
                    break;

                case 0x0015:
                    opcode_fx15(opcode);
                    break;

                case 0x0018:
                    opcode_fx18(opcode);
                    break;

                case 0x001E:
                    opcode_fx1e(opcode);
                    break;

                case 0x0029:
                    opcode_fx29(opcode);
                    break;

                case 0x0033:
                    opcode_fx33(opcode);
                    break;

                case 0x0055:
                    opcode_fx55(opcode);
                    break;

                case 0x0065:
                    opcode_fx65(opcode);
                    break;
            }
        }

        /// @brief 0x00E0: Clears the screen.
        /// @param opcode The opcode.
        void opcode_00e0(std::uint16_t opcode)
        {
            _gfx.fill(0);
            _counter += 2;
        }

        /// @brief 0x00EE: Returns from a subroutine.
        /// @param opcode The opcode.
        void opcode_00ee(std::uint16_t opcode)
        {
            _counter = _stack[--_stack_ptr];
            _counter += 2;
        }

        /// @brief 0x1NNN: Jump to address NNN.
        /// @param opcode The opcode.
        void opcode_1nnn(std::uint16_t opcode)
        {
            _counter = (opcode & 0x0FFF);
        }

        /// @brief 0x2NNN: Calls subroutine at NNN.
        /// @param opcode The opcode.
        void opcode_2nnn(std::uint16_t opcode)
        {
            _stack[_stack_ptr++] = _counter;
            _counter = (opcode & 0x0FFF);
        }

        /// @brief 0x3XNN: Skips the next instruction if register X == NN.
        /// @param opcode The opcode.
        void opcode_3xnn(std::uint16_t opcode)
        {
            auto x = (opcode & 0x0F00) >> 8;
            auto nn = (opcode & 0x00FF);

            if (_registers[x] == nn)
                _counter += 4;
            else
                _counter += 2;
        }

        /// @brief 0x4XNN: Skips the next instruction if reigster X != NN.
        /// @param opcode The opcode.
        void opcode_4xnn(std::uint16_t opcode)
        {
            auto x = (opcode & 0x0F00) >> 8;
            auto nn = (opcode & 0x00FF);

            if (_registers[x] != nn)
                _counter += 4;
            else
                _counter += 2;
        }

        /// @brief 0x5XY0: Skips the next instruction if register X == register Y.
        /// @param opcode The opcode.
        void opcode_5xy0(std::uint16_t opcode)
        {
            auto x = (opcode & 0x0F00) >> 8;
            auto y = (opcode & 0x00F0) >> 4;

            if (_registers[x] == _registers[y])
                _counter += 4;
            else
                _counter += 2;
        }

        /// @brief 0x6XNN: Sets register X = NN.
        /// @param opcode The opcode.
        void opcode_6xnn(std::uint16_t opcode)
        {
            auto x = (opcode & 0x0F00) >> 8;
            auto nn = (opcode & 0x00FF);

            _registers[x] = nn;
            _counter += 2;
        }

        /// @brief 0x7XNN: Adds NN to register X.
        /// @param opcode The opcode.
        void opcode_7xnn(std::uint16_t opcode)
        {
            auto x = (opcode & 0x0F00) >> 8;
            auto nn = (opcode & 0x00FF);

            _registers[x] += nn;
            _counter += 2;
        }

        /// @brief 0x8XY0: Sets register X = register Y.
        /// @param opcode The opcode.
        void opcode_8xy0(std::uint16_t opcode)
        {
            auto x = (opcode & 0x0F00) >> 8;
            auto y = (opcode & 0x00F0) >> 4;

            _registers[x] = _registers[y];
            _counter += 2;
        }

        /// @brief 0x8XY1: Sets register X = register X OR register Y.
        /// @param opcode The opcode.
        void opcode_8xy1(std::uint16_t opcode)
        {
            auto x = (opcode & 0x0F00) >> 8;
            auto y = (opcode & 0x00F0) >> 4;

            _registers[x] |= _registers[y];
            _counter += 2;
        }

        /// @brief 0x8XY2: Sets register X = register X AND register Y.
        /// @param opcode The opcode.
        void opcode_8xy2(std::uint16_t opcode)
        {
            auto x = (opcode & 0x0F00) >> 8;
            auto y = (opcode & 0x00F0) >> 4;

            _registers[x] &= _registers[y];
            _counter += 2;
        }
        
        /// @brief 0x8XY3: Sets register X = register X XOR register Y.
        /// @param opcode The opcode.
        void opcode_8xy3(std::uint16_t opcode)
        {
            auto x = (opcode & 0x0F00) >> 8;
            auto y = (opcode & 0x00F0) >> 4;

            _registers[x] ^= _registers[y];
            _counter += 2;
        }

        /// @brief 0x8XY4: Adds register Y to register X.
        ///        Register F is set to 1 when theres a carry, else 0.
        /// @param opcode The opcode.
        void opcode_8xy4(std::uint16_t opcode)
        {
            auto x = (opcode & 0x0F00) >> 8;
            auto y = (opcode & 0x00F0) >> 4;

            _registers[0xF] = _registers[x] > (0xFF - _registers[y]) ? 1 : 0;
            _registers[x] += _registers[y];
            _counter += 2;
        }

        /// @brief 0x8XY5: Register Y is subtracted from register X.
        ///        Register F is set to 0 when theres a borrow, else 1.
        /// @param opcode The opcode.
        void opcode_8xy5(std::uint16_t opcode)
        {
            auto x = (opcode & 0x0F00) >> 8;
            auto y = (opcode & 0x00F0) >> 4;

            _registers[0xF] = _registers[x] > _registers[y] ? 0 : 1;
            _registers[x] -= _registers[y];
            _counter += 2;
        }

        /// @brief 0x8XY6: Shifts register X to the right by 1.
        ///        Register F is set to the least significant bit before the shift.
        ///        Register Y is unused.
        /// @param opcode The opcode.
        void opcode_8xy6(std::uint16_t opcode)
        {
            auto x = (opcode & 0x0F00) >> 8;

            _registers[0xF] = _registers[x] & 0x1;
            _registers[x] >>= 1;
            _counter += 2;
        }

        /// @brief 0x8XY7: Sets register X = register Y - register X.
        //         Register F is set to 0 when theres a borrow, else 1.
        /// @param opcode The opcode.
        void opcode_8xy7(std::uint16_t opcode)
        {
            auto x = (opcode & 0x0F00) >> 8;
            auto y = (opcode & 0x00F0) >> 4;

            _registers[0xF] = _registers[x] > _registers[y] ? 0 : 1;
            _registers[x] = _registers[y] - _registers[x];
            _counter += 2;
        }

        /// @brief 0x8XYE: Shifts register X to the left by 1.
        ///        Register F is set to the most significant bit before the shift.
        ///        Register Y is unused.
        /// @param opcode The opcode.
        void opcode_8xye(std::uint16_t opcode)
        {
            auto x = (opcode & 0x0F00) >> 8;

            _registers[0xF] = _registers[x] >> 7;
            _registers[x] <<= 1;
            _counter += 2;
        }

        /// @brief 0x9XY0: Skips the next instruction if register X != register Y.
        /// @param opcode The opcode.
        void opcode_9xy0(std::uint16_t opcode)
        {
            auto x = (opcode & 0x0F00) >> 8;
            auto y = (opcode & 0x00F0) >> 4;

            if (_registers[x] != _registers[y])
                _counter += 4;
            else
                _counter += 2;
        }

        /// @brief 0xANNN: Sets the address register = NNN.
        /// @param opcode The opcode.
        void opcode_annn(std::uint16_t opcode)
        {
            _addr_ptr = (opcode & 0x0FFF);
            _counter += 2;
        }

        /// @brief 0xBNNN: Jumps to the address NNN + register 0.
        /// @param opcode The opcode.
        void opcode_bnnn(std::uint16_t opcode)
        {
            _counter = (opcode & 0x0FFF) + _registers[0x0];
        }

        /// @brief 0xCXNN: Sets register X to the bitwise AND operation between
        ///        a random number (0 to 255 inclusive) and NN.
        /// @param opcode The opcode.
        void opcode_cxnn(std::uint16_t opcode)
        {
            auto x = (opcode & 0x0F00) >> 8;
            auto nn = (opcode & 0x00FF);

            _registers[x] = _dist(_rand) & nn;
            _counter += 2;
        }

        /// @brief 0xDXYN: Draws a sprite at location (register X, register Y) that has
        ///        a width of 8 pixels and a height of N. Each row of pixels is bit-coded
        ///        starting from the memory location contained in the address register. 
        ///        Register F is set to 1 if any pixels are turned flipped from set to
        ///        unset, else 0.
        /// @param opcode The opcode. 
        void opcode_dxyn(std::uint16_t opcode)
        {
            auto x = _registers[(opcode & 0x0F00) >> 8];
            auto y = _registers[(opcode & 0x00F0) >> 4];
            auto height = (opcode & 0x000F);

            _registers[0xF] = 0;
            for (auto yline = 0; yline < height; ++yline)
            {
                auto pixel = _memory[_addr_ptr + yline];
                for (int xline = 0; xline < 8; xline++) 
                {
                    if ((pixel & (0x80 >> xline)) != 0) 
                    {
                        auto idx = x + xline + ((y + yline) * 64);
                        _registers[0xF] |= _gfx[idx];
                        _gfx[idx] ^= 1;
                    }
                }
            }
        }

        /// @brief 0xEX9E: Skips the next instruction if the key stored in 
        ///        register X is pressed.
        /// @param opcode The opcode.
        void opcode_ex9e(std::uint16_t opcode)
        {
            auto x = (opcode & 0x0F00) >> 8;

            if (_input[_registers[x]] != 0)
                _counter += 4;
            else
                _counter += 2;
        }

        /// @brief 0xEXA1: Skips the next instruction if the key stored in 
        ///        register X is not pressed.
        /// @param opcode The opcode.
        void opcode_exa1(std::uint16_t opcode)
        {
            auto x = (opcode & 0x0F00) >> 8;

            if (_input[_registers[x]] == 0)
                _counter += 4;
            else
                _counter += 2;
        }

        /// @brief 0xFX07: Sets register X = delay timer.
        /// @param opcode The opcode.
        void opcode_fx07(std::uint16_t opcode)
        {
            auto x = (opcode & 0x0F00) >> 8;

            _registers[x] = _delay_timer;
            _counter += 2;
        }

        /// @brief 0xFX0A: A key press is awaited and then stored in register X.
        /// @param opcode The opcode.
        void opcode_fx0a(std::uint16_t opcode)
        {
            auto x = (opcode & 0x0F00) >> 8;

            for (int i = 0; i < _input.size(); ++i) 
            {
                if (_input[i] != 0) 
                {
                    _registers[x] = i;
                    _counter += 2;
                    return;
                }
            }
        }

        /// @brief 0xFX15: Sets delay timer = register X.
        /// @param opcode The opcode.
        void opcode_fx15(std::uint16_t opcode)
        {
            auto x = (opcode & 0x0F00) >> 8;

            _delay_timer = _registers[x];
            _counter += 2;
        }

        /// @brief 0xFX18: Sets sound timer = register X.
        /// @param opcode The opcode.
        void opcode_fx18(std::uint16_t opcode)
        {
            auto x = (opcode & 0x0F00) >> 8;

            _sound_timer = _registers[x];
            _counter += 2;
        }

        /// @brief 0xFX1E: Adds register X to the address register. Register F
        ///        is set to 1 when there is a carry, else 0.
        /// @param opcode The opcode.
        void opcode_fx1e(std::uint16_t opcode)
        {
            auto x = (opcode & 0x0F00) >> 8;

            _registers[0xF] = _registers[x] > (0xFFFF - _addr_ptr) ? 1 : 0;
            _addr_ptr += _registers[x];
            _counter += 2;
        }

        /// @brief 0xFX29: Sets the address register to the location of the sprite
        ///        for the character in VX. Each character (0 to F) is 5 bytes long.
        /// @param opcode The opcode.
        void opcode_fx29(std::uint16_t opcode)
        {
            auto x = (opcode & 0x0F00) >> 8;

            _addr_ptr = _registers[x] * 0x5;
            _counter += 2;
        }

        /// @brief 0xFX33: Stores the binary-coded decimal representation of
        ///        register X. Each digit is stored in successive locations
        ///        starting at the address register.
        /// @param opcode The opcode.
        void opcode_fx33(std::uint16_t opcode)
        {
            auto x = (opcode & 0x0F00) >> 8;
            auto number = _registers[x];

            _memory[_addr_ptr] = number / 100;
            _memory[_addr_ptr + 1] = (number / 10) % 10;
            _memory[_addr_ptr + 2] = (number % 100) % 10;
            _counter += 2;
        }

        /// @brief 0xFX55: Stores register 0 to register X inclusive in memory
        //         starting at the location in the address register.
        /// @param opcode The opcode.
        void opcode_fx55(std::uint16_t opcode)
        {
            auto x = (opcode & 0x0F00) >> 8;

            for (int i = 0; i <= x; i++)
            {
                _memory[_addr_ptr + i] = _registers[i];
            }

            _addr_ptr += x + 1;
            _counter += 2;
        }

        /// @brief 0xFX65: Fills register 0 to register X with values from memory
        ///        starting at the location in the address register.
        /// @param opcode The opcode.
        void opcode_fx65(std::uint16_t opcode)
        {
            auto x = (opcode & 0x0F00) >> 8;

            for (int i = 0; i <= x; i++) 
            {
                _registers[i] = _memory[_addr_ptr + i];
            }

            _addr_ptr += x + 1;
            _counter += 2;
        }
    };
}

#endif
