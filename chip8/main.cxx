#include <iostream>
#include <SDL2/SDL.h>

/// The width of the window.
constexpr int SCREEN_WIDTH = 640;

/// The height of the window.
constexpr int SCREEN_HEIGHT = 480;

/// The window we'll be rendering to.
SDL_Window* window = nullptr;

/// The surface contained by the window.
SDL_Surface* surface = nullptr;

SDL_Renderer* renderer = nullptr;

SDL_Texture* texture = nullptr;

/// Initialises the SDL library and sets up variables.
void Initialise()
{
    if (SDL_Init(SDL_INIT_VIDEO) < 0) {
        std::cerr << "SDL could not initialise: " << SDL_GetError() << std::endl;
    }

    // Create window
    window = SDL_CreateWindow(
        "CHIP-8 Emulator",
        SDL_WINDOWPOS_UNDEFINED,
        SDL_WINDOWPOS_UNDEFINED,
        SCREEN_WIDTH,
        SCREEN_HEIGHT,
        SDL_WINDOW_SHOWN
    );

    // Check if window creation successful
    if (!window) {
        std::cerr << "Window could not be created: " << SDL_GetError() << std::endl;
    }

    surface = SDL_GetWindowSurface(window);
    renderer = SDL_CreateRenderer(window, -1, 0);
    texture = SDL_CreateTexture(renderer, SDL_PIXELFORMAT_RGB332, SDL_TEXTUREACCESS_STATIC, 64, 32);
}

void Deinitialise() {
    SDL_DestroyTexture(texture);
    SDL_DestroyRenderer(renderer);
    SDL_DestroyWindow(window);
    SDL_Quit();
}

int main(int argc, char** argv)
{
    bool quit = false;
    SDL_Event event;
    Chip8 chip8(argv[1]);
    chip8.Start();

    while (!quit)
    {
        SDL_UpdateTexture(texture, nullptr, &chip8.Graphics, 64 * sizeof(uint8_t));
        SDL_WaitEvent(&event);
        switch (event.type)
        {
        case SDL_QUIT:
            chip8.Stop();
            quit = true;
            break;
        }

        if (chip8.DrawFlag)
        {
            SDL_RenderClear(renderer);
            SDL_RenderCopy(renderer, texture, nullptr, nullptr);
            SDL_RenderPresent(renderer);
            chip8.DrawFlag = false;
        }
    }
    return 0;
}